package Tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.beust.jcommander.Parameters;

public class BaseTest {
	
	public WebDriver driver;
	//public HomePage homePage;
	String browser = System.getProperty("browser");

	@Parameters({"url"})
	@BeforeClass
	public void setup(String url) throws IOException {
		//System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized"); // open Browser in maximized mode
		options.addArguments("--headless"); // applicable to windows os only
		//driver = new ChromeDriver(options);
		//driver.manage().window().maximize();
	    FirefoxBinary firefoxBinary = new FirefoxBinary();
	    firefoxBinary.addCommandLineOptions("--headless");
	    FirefoxOptions firefoxOptions = new FirefoxOptions();
	    firefoxOptions.setBinary(firefoxBinary);
		
		if(browser != "" & browser != null ) {
			if(browser.equalsIgnoreCase("chrome")) {
				driver = new ChromeDriver(options);
				driver.manage().window().maximize();
			}
			else if(browser.equalsIgnoreCase("Firefox")) {
				driver = new FirefoxDriver(firefoxOptions);
				driver.manage().window().maximize();

			}
			
		}
		else {

			driver = new ChromeDriver(options);
			driver.manage().window().maximize();
		}
		
		
		
		driver.manage().window().setSize(new Dimension(1440, 900));

		driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);	
		driver.get(url);	
		
	}
	
	
	@AfterClass
	public void tearDown() throws InterruptedException {	
		
		Thread.sleep(5000);
		
		driver.quit();
	}

}


